/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import { NavigationContainer } from '@react-navigation/native';
import 'react-native-gesture-handler' 
import React from 'react';
import { Provider } from 'react-redux';
import store from './src/redux/store/store';
import { AuthNavigator } from './src/routes/AuthNavigator';

const App = () => {
  return (
    <Provider store={ store }>
      <NavigationContainer>
        <AuthNavigator />
      </NavigationContainer>

    </Provider>
  );
};

export default App;
