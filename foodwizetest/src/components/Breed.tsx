import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import React, { useRef, useState } from 'react'
import { IBreedsList } from '../interfaces/breedInterface';
import { colors } from '../theme/appTheme';
import { Transition, Transitioning } from 'react-native-reanimated';
import { useDispatch, useSelector } from 'react-redux';
import { breedState } from '../redux/reducers/breedReducer';
import Icon from 'react-native-vector-icons/Ionicons';
import { addFavorite, removeFavorite } from '../redux/actions/breeds';
import { useNavigation } from '@react-navigation/native';

interface Props {
    breed: IBreedsList;
    height?: number;
    width?: number
}
const transition = (
  <Transition.Together>
    <Transition.In type='fade' durationMs={200} />
    <Transition.Change />
    <Transition.Out type='fade' durationMs={200} />
  </Transition.Together>
);
export const Breed = ({ breed, width = 300 }: Props) => {
  //redux 
  const favoriteBreeds = useSelector(({ breed }: { breed: breedState}) => breed.favoriteBreeds)
  const dispatch = useDispatch()
  //end redux
  const [showList, setShowList] = useState(false);
  const ref = useRef<any>();
  const navigation = useNavigation()

  const handleFavorite = (subBreed :string)=> {
    favoriteBreeds.includes(subBreed) ? dispatch( removeFavorite( subBreed ) ) : dispatch( addFavorite(subBreed) )
  }
  return (
      <Transitioning.View
        ref={ref}
        transition={transition}
        style={{
          minWidth: width,
          
        }}
      >

      <View style={
        styles.breedContainer
      }>
        <View
          style={ styles.titleContainer }
        >
          <Text onPress={() => navigation.navigate('BreedDescription', {
            ...breed
          }) } style={ styles.breedTitle }>
            { breed.title }
          </Text>
        </View>

        <View style={ styles.favoriteContainer }>
          <TouchableOpacity
            onPress={ ()=> {
              ref.current.animateNextTransition();
              setShowList(!showList)
            }}
          >
            {
              breed.breeds.length > 0 && (
                <Text style={ styles.toggleButton } >
                  { showList && <Icon  name={ 'remove-outline'} color={ 'black' } size={ 25 } /> }
                  { !showList && <Icon  name={ 'add-outline'} color={ 'black' } size={ 25 } /> }
                </Text>
              )
              
            }
          </TouchableOpacity>
        </View>
          { showList && (
              breed.breeds.map( ( subBreed,index ) => ( 
                <TouchableOpacity
                  key={ index }
                  onPress={ ()=> handleFavorite(subBreed) }
                >
                  <View style={ styles.subBreedsContainer }>
                    <View style={{ flex:0.7, marginLeft: 18 }}>
                      <Text style={{textAlign: 'center'}} key={subBreed}>{ subBreed }</Text>
                    </View>
                    <View>
                      <Icon style={ styles.favoriteIcon } name={ favoriteBreeds.includes(subBreed) ? 'star' : 'star-outline'} color={ 'black' } size={ 25 } />
                      
                    </View>
                  </View>

                </TouchableOpacity>
              ))
            ) 
          }
      </View>
    </Transitioning.View>
  )
}

const styles = StyleSheet.create({
  image: {
    borderRadius: 20,
    flex: 1,
  },
  titleContainer: {
    flex: 0.8,
    justifyContent: 'flex-start',
  },
  favoriteContainer: {
    alignItems: 'center',
    flex: 0.2,
  },
  toggleButton: {
    color: 'yellow',
    fontWeight: 'bold',    
    fontSize: 20
  },
  favoriteIcon: {
    marginLeft: 20,
    color:'yellow'
  },
  breedContainer: {
    flex: 0.8,
    marginHorizontal: 8,
    borderRadius: 5,
    flexDirection: 'column',
    backgroundColor: colors.primary,
  },
  breedTitle: {
    textAlign: 'center',
    marginTop: 15,
    color: 'white',
    fontSize: 20,
    textTransform: 'capitalize',
  },
  subBreedsContainer: {
    flexDirection: 'row',
    flex: 1,
    justifyContent:'center',
    alignItems:'center'
  }
});