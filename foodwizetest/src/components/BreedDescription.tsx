import { View, Text, Button, TouchableOpacity, Image } from 'react-native';
import React, { useEffect, useState } from 'react';
import { colors } from '../theme/appTheme';
import Icon from 'react-native-vector-icons/Ionicons';
import { BottomTabScreenProps } from '@react-navigation/bottom-tabs';
import { RootStackParams } from '../routes/AuthNavigator';
import { docCeo } from '../api/docCeo';
import { ScrollView } from 'react-native-gesture-handler';

interface Props extends BottomTabScreenProps<RootStackParams, 'BreedDescription'> {}
export default function BreedDescription( { navigation, route }: Props ) {
    const [imageUri, setImageUri] = useState('')

  useEffect(() => {
    getImage()
  }, [])
    
    const getImage = async () => {
      try {
          const resp = await docCeo.get(`/breed/${route.params.title}/images`)
          setImageUri(resp.data.message)
          
      } catch (error) {
          console.log(error)
      } 
    
  }
  
  useEffect(() => {
    navigation.setOptions({
      headerLeft: ()=> (
/*         <Button 
          title='menu'
          onPress={ ()=> navigation.toggleDrawer() }
        ></Button> */
        <TouchableOpacity
          activeOpacity={0.3}
          onPress={ ()=> navigation.goBack() }
          
        >
          <Icon name='arrow-back' color={ colors.primary } size={ 40 } />
        </TouchableOpacity>
      )
    })
  
  }, []);

  return (
    <View style={{flex: 1}} >

      <View style={{ flexDirection: 'row', flex: 1, borderWidth: 5, alignItems: 'center' }}>   
        {imageUri.length > 0 && (
            <Text style={{fontSize: 25, textAlign: 'center'}}>
              { route.params.title }
             <Image source={{ uri: imageUri[0] }} style={{ width: 150, height: 150 }} />
            </Text>
        )}
        {
            route.params.breeds.length > 0 && (
                      <ScrollView style={{ marginTop: 40 }}>
                    {
                        route.params.breeds.map((subBreed, index) => {
                            return (
                                <View key={index + subBreed}>
                                    <Text>{subBreed}</Text>
                                    <Image source={{ uri: imageUri[index +1] }} style={{ width: 150, height: 150 }} />
                                </View>
                            )
                        })
                    }
                          
                </ScrollView>
            )
        }    
          </View>
      
    </View>
  );
}
