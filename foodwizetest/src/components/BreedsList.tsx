import React from 'react'
import { FlatList, StyleSheet, View } from 'react-native';
import { IBreedsList } from '../interfaces/breedInterface';
import { Breed } from './Breed';


interface Props {  breeds: IBreedsList[] }
export const BreedsList = ({ breeds }:Props) => {
  return (
    <View
      style={ styles.breedsContainer }
    >
    <FlatList 
      data={ breeds }
      renderItem={ ( { item, index } ): any => (
        <Breed breed={ item }  key={ index }/>
      )}
      initialNumToRender={ breeds.length } 
      ItemSeparatorComponent={() => (
        <View style={{ height: 10 }} />
      )}
      showsVerticalScrollIndicator= { false }
      keyExtractor={ ( item, index )=> index.toString() }
    />
  </View>
  )
}


const styles = StyleSheet.create({
  breedsContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  }
})