import { Text, StyleSheet, View, TouchableOpacity } from 'react-native'
import React from 'react'
import { colors } from '../../theme/appTheme'
import Icon from 'react-native-vector-icons/Ionicons';
import { AnyAction, Dispatch } from '@reduxjs/toolkit';
import { addFavorite, removeFavorite } from '../../redux/actions/breeds';

interface Props {
    breed: string,
    favoriteBreeds: string[],
    dispatch: Dispatch<AnyAction>
}
export const BreedWithIconRow = ({ breed, favoriteBreeds, dispatch } :Props ) => {

    const handleFavorite = (subBreed :string)=> {
        favoriteBreeds.includes(subBreed) ? dispatch( removeFavorite( subBreed ) ) : dispatch( addFavorite(subBreed) )
    }
    
    return (
      <View style={ styles.container }>
          <TouchableOpacity
          key={ breed + 1 }
          onPress={ ()=> handleFavorite( breed )  }
          style={ styles.breedsContainer }
        >
          <View style={{ flex:0.7, marginLeft: 18 }}>
              <Text style={{textAlign: 'center'}} key={breed}>{ breed }</Text>
          </View>
          
          <View>
              <Icon style={ styles.favoriteIcon } name={ favoriteBreeds.includes(breed) ? 'star' : 'star-outline'} color={ 'black' } size={ 25 } />
          </View>

        </TouchableOpacity>

      </View>
    )

}

const styles = StyleSheet.create({
    favoriteIcon: {
      marginLeft: 20,
      color:'yellow'
    },
    breedsContainer: {
      flexDirection: 'row',
      flex: 1,
      justifyContent:'center',
      alignItems:'center',
    },
    container: {
      marginHorizontal: 8,
      borderRadius: 5,
      flexDirection: 'column',
      backgroundColor: colors.primary,
      width: 300,
      height: 60,
      marginBottom: 5,
    }
});