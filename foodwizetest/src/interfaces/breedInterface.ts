export interface IBreeds {
    message: { [key: string]: string[] };
    status:  string;
}


export interface IBreedsList { 
    title: string; 
    breeds: string[]; 
}