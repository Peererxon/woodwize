import { AnyAction } from "@reduxjs/toolkit";
import { IUserInterface } from "../../interfaces/userInterface";
import { LOGIN, LOGOUT } from "../constans/constans";

export const login = ( payload: IUserInterface ) : AnyAction => {
    return {
        type: LOGIN,
        payload
    }
}

export const logout = ( ) : AnyAction => {
    return {
        type: LOGOUT
    }
}