import { ADD_BREED_FAVORITE, REMOVE_BREED_FAVORITE } from "../constans/constans";

export const addFavorite = ( breed: string ) => {
    return {
        type : ADD_BREED_FAVORITE,
        payload: breed
    }
}

export const removeFavorite = ( favoriteBreed: string ) => {
    return {
        type: REMOVE_BREED_FAVORITE,
        payload : favoriteBreed
    }
}