import { AnyAction,Reducer } from "@reduxjs/toolkit";
import { IUserInterface } from "../../interfaces/userInterface";
import { LOGOUT, LOGIN } from "../constans/constans";

export interface IAuthState {
    user: IUserInterface,
    isAuth: boolean
}

const initialState: IAuthState = {
    user: {
        email: '',
        name: ''
    },
    isAuth: false
}
const authReducer: Reducer<IAuthState,AnyAction> = (state = initialState, action: AnyAction) => {
    switch ( action.type ) {
        case LOGIN:
            return {
                ...state,
                user: action.payload,
                isAuth: true
            }
    
        case LOGOUT:
            return {
                ...state,
                user: initialState.user,
                isAuth: false
            }
        default:
            return state
    }
}
export default authReducer;