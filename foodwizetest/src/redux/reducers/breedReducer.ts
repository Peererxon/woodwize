import { AnyAction,Reducer } from "@reduxjs/toolkit";
import { ADD_BREED_FAVORITE, REMOVE_BREED_FAVORITE } from "../constans/constans";

export interface breedState {
    favoriteBreeds: string[]
}

type actionTypes = 
    { type: typeof ADD_BREED_FAVORITE, payload: string } |
    { type: typeof REMOVE_BREED_FAVORITE, payload: string }


const initialState = {
    favoriteBreeds: []
} as breedState
const breedReducer: Reducer<breedState, AnyAction> = (state = initialState, action: AnyAction) => {
    
    switch(action.type) {
        case ADD_BREED_FAVORITE:
            
            return {
                ...state,
                favoriteBreeds: [ ...state.favoriteBreeds, action.payload ]
            };

        case REMOVE_BREED_FAVORITE:
            return {
                ...state,
                favoriteBreeds: state.favoriteBreeds.filter( favoriteBreed => favoriteBreed !== action.payload )
            }
        default:
            return state;
    }
}
export default breedReducer;