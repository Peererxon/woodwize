import { configureStore } from '@reduxjs/toolkit'
import authReducer from '../reducers/authReducer';
import breedReducer from '../reducers/breedReducer';
// ...

const store = configureStore({
  reducer: {
    breed: breedReducer,
    auth: authReducer
  },
})

export default store