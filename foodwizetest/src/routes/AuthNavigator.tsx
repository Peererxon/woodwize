import { createStackNavigator } from '@react-navigation/stack';
import React from 'react'
import { useSelector } from 'react-redux';
import BreedDescription from '../components/BreedDescription';
import { IBreedsList } from '../interfaces/breedInterface';
import { IAuthState } from '../redux/reducers/authReducer';

import Login from '../screen/Login';
import { Tabs } from './Tabs';
const Stack = createStackNavigator<RootStackParams>();

export type RootStackParams = {
    Login: undefined,
    Tabs: undefined,
    BreedDescription: IBreedsList,
}

export const AuthNavigator = () => {
    const isAuth = useSelector(({ auth }: { auth: IAuthState } ) => auth.isAuth)

    // More modular for latter if we want to add more logic/routes to our auth flux
    const autenticateRoutes = (
        <Stack.Group>
          <Stack.Screen name='Tabs' component={Tabs} />
          <Stack.Screen name='BreedDescription' component={BreedDescription} />
        </Stack.Group>
    );

    const noAuthRoutes = (
        <Stack.Group>
            <Stack.Screen name="Login" component={Login} />
        </Stack.Group>
    )
  return (
    <Stack.Navigator
      screenOptions={{
        //elevation eimina la linea de android
        headerStyle: {
          elevation: 0,
          shadowColor: 'transparent',
        },
        cardStyle: {
          backgroundColor: 'white',
        },
      }}>
          {
              isAuth ? autenticateRoutes :  noAuthRoutes
          }
    </Stack.Navigator>
  );
}