import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { colors } from '../theme/appTheme';
import { Text, View } from 'react-native';
import Breeds from '../screen/Breeds';
import { Profile } from '../screen/Profile';

const BottomTabs = createBottomTabNavigator();

export const Tabs = () => {
  return (
    <BottomTabs.Navigator
      sceneContainerStyle={{
        backgroundColor: 'gray'
      }}
      >
      <BottomTabs.Screen name="Breeds" options={{ title: 'LS', tabBarActiveTintColor: 'orange', tabBarIcon: ( ) => <Text style={{ color: colors.primary  }}>List</Text> }} component={ Breeds } />
      <BottomTabs.Screen name="Profile"

        options={
          {
            title: 'Profile', tabBarActiveTintColor: 'orange',
            tabBarIcon: () => (<Text style={{ color: colors.primary }}>Profile</Text>),
            headerShown: false,
          }
        }
        component={Profile} />
    </BottomTabs.Navigator>
  );
}