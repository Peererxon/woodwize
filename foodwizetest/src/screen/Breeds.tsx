import { ActivityIndicator, Text, View} from 'react-native'
import React, { useEffect, useState } from 'react'
import { useSafeAreaInsets } from 'react-native-safe-area-context'
import { docCeo } from '../api/docCeo'
import { IBreeds, IBreedsList } from '../interfaces/breedInterface'
import { BreedsList } from '../components/BreedsList'
import { colors } from '../theme/appTheme'

export default function Breeds(){
    const { top } = useSafeAreaInsets()
    const [breeds, setBreeds] = useState<IBreedsList[]>([])
    useEffect(() => {
      getAllBreed()
    }, [])

    const getAllBreed = async () => {
        const breeds = await docCeo.get<IBreeds>('/breeds/list/all')
        const keysOfmessage = Object.keys( breeds.data.message || {} )
        const breedsMapped = []
        if (keysOfmessage.length > 0 ) {
            for (let index = 0; index < keysOfmessage.length; index++) {
                const breedKey = keysOfmessage[index]
                const breed = {
                    title: breedKey,
                    breeds: breeds.data.message[breedKey]
                }
                breedsMapped.push(breed)
            }

        }
        setBreeds(breedsMapped)
    }
    
    if ( breeds.length === 0 ){
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}>
              <ActivityIndicator  color={ colors.primary } size={100} />
            </View>
        )
    }

    return (
      <View style={{
          marginTop: top + 10
      }}
      >
        <BreedsList breeds={breeds}/>

      </View>
    )
}