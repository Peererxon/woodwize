import React, { useState } from "react";
import { Button, SafeAreaView, StyleSheet, TextInput, View } from "react-native";
import { useDispatch } from "react-redux";
import { login } from "../redux/actions/auth";
import { colors } from "../theme/appTheme";

const Login = () => {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const dispatch = useDispatch()
    const handleClick = () => {
        const user = {
            name,
            email
        }
        dispatch( login( user ) )
    }
    return (
      <SafeAreaView>
        <TextInput
          style={styles.input}
          onChangeText={ setName }
          value={name}
          placeholder="name"
        />
        <TextInput
          style={styles.input}
          onChangeText={ setEmail }
          value={email}
          placeholder="Email"
          keyboardType="email-address"
        />
        
        <View style={ styles.submitContainer }>
            <Button
                title="Login"
                color={colors.primary}
                disabled={(name === '' || email === '')}
                onPress={ handleClick }    
            />
        </View> 
      </SafeAreaView>
    );
};
const styles = StyleSheet.create({
    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        padding: 10,
    },
    submitContainer: {
        justifyContent: 'center',
        width: '80%',
        marginLeft: '10%'
    }
});
    
export default Login;