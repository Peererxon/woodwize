import { Text, StyleSheet, View, FlatList, Button } from 'react-native'
import React from 'react'
import { BreedWithIconRow } from '../global/components/BreedWithIconRow'
import { useDispatch, useSelector } from 'react-redux'
import { breedState } from '../redux/reducers/breedReducer'
import { colors } from '../theme/appTheme'
import { IUserInterface } from '../interfaces/userInterface'
import { logout } from '../redux/actions/auth'

export const Profile = () => {
  
  const favoriteBreeds = useSelector(({ breed }: { breed: breedState }) => breed.favoriteBreeds);
  const userData: IUserInterface = useSelector(({ auth }: { "auth": { isAuth: boolean, user: { name: string, email: string } } } ) => auth.user);
  const { email, name } = userData
  const dispatch = useDispatch()
  const handleLogout = () => {
    dispatch( logout() )
  }
    return (
      <View style={{ flex: 1 }}>
        <View style={ styles.userProfile }>
          <View style={{ flex: 0.5, alignItems: 'center' }}>
            <Text style={ styles.text }>Welcome! {name}</Text>
            <Text style={ styles.text }>{email}</Text>
          </View>
          <View style={{ flex: 0.5, alignItems: 'center' }}>
            <Button
              title='Logout'
              color={colors.primary}
              onPress={ handleLogout }
            />
          </View>
        </View>
        <View style={ styles.wrapperFavoriteList }>
          {
            favoriteBreeds.length > 0 ? (
              <View>
                <FlatList 
                  data={ favoriteBreeds }
                  contentContainerStyle={{ alignItems: 'center' }}
                  renderItem={ ( { item, index } ): any => (
                    <BreedWithIconRow breed={item} dispatch={ dispatch } key={item + index} favoriteBreeds={favoriteBreeds} />
                  )}
                  ItemSeparatorComponent={() => (
                    <View style={{ height: 10 }} />
                  )}
                  showsVerticalScrollIndicator= { false }
                  keyExtractor={ ( item, index )=> index.toString() }
                />
              </View>
            )
            : <Text style={{ textAlign: 'center' }}>Favorite list empy</Text>
          }
        
        </View>
      </View>
    )
}

const styles = StyleSheet.create({

  breedContainer: {
    flex: 0.8,
    marginHorizontal: 8,
    borderRadius: 5,
    flexDirection: 'column',
    backgroundColor: colors.primary,
  },
  wrapperFavoriteList: {
    flex: 3/4,
    textAlign: 'center',
    marginTop: 10
  },
  userProfile: {
    flex: 1/4,
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: 'white'
  },
  text: {
    fontSize: 20,
    textAlign: 'center',
    color: 'black'
  }
})